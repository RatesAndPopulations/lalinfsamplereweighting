.. lalinfsamplereweighting documentation master file, created by
   sphinx-quickstart on Mon Mar 18 10:52:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lalinfsamplereweighting's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: rewtlisamples
  :members: log_lirewt_factor

.. automodule:: approxprior
  :members: approx_dl_prior

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
