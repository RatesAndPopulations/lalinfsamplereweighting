# Re-weighting of LALInference samples into the comoving frame.
# Copyright (C) 2019 Colm Talbot <colm.talbot@ligo.org>, Daniel Wysocki
# <daniel.wysocki@ligo.org>, Richard O'Shaughnessy <richard.oshaughnessy@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

r"""This module contains functions to re-weight samples from LALInference, which
uses a prior that is flat in source-frame masses and proportional to
:math:`d_L^2` into the comoving frame.  The necessary re-weighting factor is

.. math::

    \frac{1}{p_{LI}\left( m_1^\mathrm{det}, m_2^\mathrm{det}, d_L \right) } \left| \frac{\partial \left(m_1^{\mathrm{source}}, m_2^{\mathrm{source}}, V_C \right)}{\partial \left( m_1^{\mathrm{det}}, m_2^{\mathrm{det}}, d_L\right)} \right|

where :math:`V_C` is the comoving volume.  This factor is required so that
subsequent analyses can impose their priors in the comoving frame (i.e. priors
in :math:`m_1^\mathrm{source}`, :math:`m_2^\mathrm{source}`, :math:`V_C`), and
they will automatically be converted to the appropriate density for the LI
sampling in the :math:`m_1^\mathrm{det}`, :math:`m_2^\mathrm{det}`, :math:`d_L`
space.

The Jacobian is triangular, so can be implemented as a product of three factors

.. math::

    \frac{\partial m_1^{\mathrm{source}}}{\partial m_1^{\mathrm{det}}} \frac{\partial m_2^{\mathrm{source}}}{\partial m_2^{\mathrm{det}}} \frac{\partial V_C}{\partial d_L}

The first two factors are just inverse :math:`1+z`, while the final factor can
be derived from

.. math::

    \frac{\partial V_C}{\partial d_L} = \frac{\partial V_C}{\partial z} \left[ \frac{\partial d_L}{\partial z} \right]^{-1}

where

.. math::

    \frac{\partial d_L}{\partial z} = d_C + \left( 1+z\right)\frac{d_H}{E(z)}

in a flat universe (:math:`d_H` is the Hubble distance, and :math:`E(z)` is the
standard integrand; see `Hogg (1999)
<https://ui.adsabs.harvard.edu/#abs/1999astro.ph..5116H/abstract>`_ for more
information).

"""

import numpy as np
from scipy.interpolate import interp1d

from astropy import cosmology, units

def log_lirewt_factor(li_dls):
    # define various things for weight calculation
    dls = np.linspace(1, 10000)
    z_of_dl = np.array([
        cosmology.z_at_value(cosmology.Planck15.luminosity_distance, dl *
                             units.Mpc) for dl in dls])

    dl_to_z = interp1d(dls, z_of_dl)

    dvl_dz = np.gradient(dls**3, z_of_dl)
    dvc_dl = cosmology.Planck15.differential_comoving_volume(z_of_dl).value

    j_dl_sq_of_dl = interp1d(dls, (1 + z_of_dl)**2 * dvl_dz)
    j_dl_sq_of_z = interp1d(z_of_dl, (1 + z_of_dl)**2 * dvl_dz)

    p_ucv_of_z = interp1d(z_of_dl, dvc_dl / (1 + z_of_dl))

    reds = dl_to_z(li_dls)
    old_prior_weight = j_dl_sq_of_z(reds)
    comoving_volume_weight = p_ucv_of_z(reds)

    log_rewt = np.log(old_prior_weight / comoving_volume_weight / (1 + reds))

    return -log_rewt
