#!/usr/bin/env python
"""
Computes posterior sample weights needed to modify the prior used by
lalinference.  Reweighting formulas are in rewtlisamples.py, this script only
performs the I/O.
"""

import os
import numpy

import itertools

from rewtlisamples import log_lirewt_factor


events = ['GW150914', 'LVT151012', 'GW151226', 'GW170104', 'GW170608',
          'GW170729', 'GW170809', 'GW170814', 'GW170818', 'GW170823']

approximants = ['IMRPhenomPv2', 'SEOBNRv3']

for approx, event in itertools.product(approximants, events):
    # Filenames for original lalinference posterior samples, and weights
    # we will compute for those.
    source_fname = os.path.join(approx, "{}.dat".format(event))
    dest_fname = os.path.join(
        "{}_weights_only".format(approx),
        "{}.dat".format(event),
    )

    # Load in the original lalinference posteriors.
    orig_post = numpy.genfromtxt(source_fname, names=True)
    # Pull out the luminosity distance field.
    li_dls = orig_post['distance']

    # Compute `log(1/weights)`, and then transform that to `weights`.
    log_inv_weights = log_lirewt_factor(li_dls)
    weights = numpy.exp(-log_inv_weights)

    # Save `weights` to a text file.
    numpy.savetxt(dest_fname, weights, header="weight", comments="")
