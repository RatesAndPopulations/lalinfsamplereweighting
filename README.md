# LALInfSampleReweighting

Re-weighting functions for LALInference priors into uniform in the comoving frame.

This code is released under the GPL v3 (see `LICENSE.md` in this directory for details).

See `_build/html/index.html` for documentation.
