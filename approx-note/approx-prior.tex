\documentclass[modern]{aastex62}

\usepackage{acro}
\usepackage{amsmath}

\newcommand{\dd}{\mathrm{d}}
\newcommand{\diff}[2]{\frac{\dd #1}{\dd #2}}

\DeclareAcronym{GW}{
  short = GW,
  long = gravitational wave
}


\begin{document}

\title{A Simple Luminosity Distance Prior Incorporating Cosmology}
\author{Will M. Farr}
\email{will.farr@ligo.org}
\affiliation{Department of Physics and Astronomy, Stony Brook University, Stony Brook NY 11794, United States}
\affiliation{Center for Computational Astrophysics, Flatiron Institute, 162 5th Ave, New York NY 10010, United States}

\begin{abstract}
%
  With detections of binary black holes in Advanced LIGO and Advanced Virgo
  reaching cosmological distances ($z \gtrsim 1$) single-event analyses should
  use prior in luminosity distance appropriate to a non-Euclidean cosmology.
  This document defines a prior that is uniform in the comoving frame of a Flat
  $\Lambda$CDM cosmology to $\sim 1\%$ but is a simple expression that can be
  implemented (and removed, for population analyses) in any code without needing
  to perform cosmological integrals.
%
\end{abstract}

\section{Approximate Prior}

To obtain un-biased estimates of the properties of a \ac{GW} event the
appropriate prior to use is the distribution of the population of events
\citep{2010ApJ...725.2166H,2010PhRvD..81h4029M,2019MNRAS.tmp..870M}.
Unfortunately, this distribution is generally only available following an
analysis of the full population of detection \ac{GW} events\footnote{For some
parameters, sky location or inclination, for example, the population
distribution is known a priori.}.  In the absence of a prior based on the
population, it is reasonable to choose a prior that is relatively
``uninformative'' about the parameters of interest, preferably one without small
``tails.''  Such a prior will make follow-on population analyses---which,
generally, must ``un-do'' the single-event prior by dividing out its
density---more stable, and also prevent strong prior-dependent shifts in
reported single-event parameters.

Here we propose a prior for the luminosity distance \citep{1999astro.ph..5116H}
for use in LIGO parameter estimation analysis
\citep{2015PhRvD..91d2003V,2018arXiv181112907T} that meets the following
requirements:
%
\begin{enumerate}
%
  \item \label{item:measured-directly} The prior is expressed in terms of a
  variable we measure directly: $d_L$.  For example, we also choose to impose a
  prior on the \emph{detector} frame masses rather than the (derived, but more
  physical) source-frame masses.
%
  \item \label{item:broad-uninformative} The prior is broad and uninformative
  over reasonable physical models for the population.  This will minimize bias
  in single-event parameters and maximize sample coverage for follow-up analyses
  that re-weight to a new population.
%
  \item \label{item:simple} The prior is simple to state and implement.  This
  facilitates the use (and exploration!) of alternate priors and lightweight
  follow-on population analyses.
%
  \item \label{item:proper} The prior is \emph{proper}; that is, it is
  (proportional to) a probability distribution that integrates to 1.  This
  facilitates comparison and computation of evidences and makes sampling,
  particularly parallel-tempered sampling, more stable and efficient.
%
\end{enumerate}
%
Our current (as of this writing) prior, $p\left( d_L \right) \propto d_L^2$,
satisfies requirements \ref{item:measured-directly} and \ref{item:simple}, fails
to meet \ref{item:broad-uninformative}, and only meets \ref{item:proper} if a
maximum distance is imposed.  It would be appropriate in a Euclidean universe,
but the universe is not Euclidean on large scales.  The existing prior fails to
meet \ref{item:broad-uninformative} because $d_L^2$ is a very bad approximation
to any reasonable distribution of mergers outside the local universe ($z \gtrsim
0.25$); Figure \ref{fig:dl2-is-bad} compares a $d_L^2$ prior to a prior that
corresponds to a uniform merger rate in the comoving frame (see below).

\begin{figure}
  \plotone{dl2-is-bad}
%
  \caption{\label{fig:dl2-is-bad} Redshift distribution corresponding to a prior
  representing a uniform merger rate in the comoving frame (blue curve) and a
  luminosity distance prior proportional to $d_L^2$ (orange curve).  Priors
  corresponding to reasonable merger rates in the comoving frame differ markedly
  from a constant merger rate in a Euclidean universe.}
%
\end{figure}

A reasonable choice of prior that would satisfy all requirements but
\ref{item:simple} would be a constant merger rate in the comoving frame
\citep{2016ApJ...833L...1A}:
%
\begin{equation}
  \label{eq:uniform-source-frame}
  p\left( d_L \right) \propto \diff{V}{z} \frac{1}{1+z} \left( \diff{d_L}{z} \right)^{-1}.
\end{equation}
%
The final factor, a Jacobian to convert a density in $z$ to $d_L$, is (in a
spatially flat universe; \citet{1999astro.ph..5116H})
%
\begin{equation}
  \diff{d_L}{z} = d_C + (1+z) \frac{d_H}{E(z)},
\end{equation}
%
where $d_C$ is the comoving distance, $d_H = c/H_0$ is the Hubble distance, and
$E(z)$ is the normalized Hubble parameter at redshift $z$.  To compute the prior
in Eq.\ \eqref{eq:uniform-source-frame} one must choose a set of cosmological
parameters ($H_0$, $\Omega_M$, $w$) in a flat $w$CDM cosmology, and then perform
an integral to compute $V(z)$; libraries exist in some languages that
automatically compute such cosmological quantities
\citep[e.g.~\texttt{astropy}~][]{astropy:2013,astropy:2018}, but there are no
simple closed-form expressions if a library import is not available or desired.

We propose the rational function prior
%
\begin{equation}
  \label{eq:rf-dL-prior}
  p\left( d_L \right) \propto \frac{d_L^2}{\sum_{i=0}^{4} c_i d_{L,9}^i}
\end{equation}
%
with
%
\begin{equation}
  \mathbf{c} = \begin{pmatrix}
1.012306, 1.136740, 0.262462, 0.016732, 0.000387
\end{pmatrix},
\end{equation}
%
and $d_{L,9}$ the luminosity distance in units of Gpc.  This function
approximates the prior in Eq.\ \eqref{eq:uniform-source-frame} with parameters
chosen from the \texttt{Planck15} cosmology \citep[Table 4, TT, TE, EE + lowP +
lensing + ext]{2016A&A...594A..13P} from \texttt{astropy}
\citep{astropy:2013,astropy:2018} to better than 1\% for $0 \leq z < 4$, but has
a simple closed form expression, and therefore satisfies all our requirements.
Figure \ref{fig:prior-compare} shows both priors; they are indistinguishable to
the eye.

\begin{figure}
    \plotone{prior-compare}
%
  \caption{\label{fig:prior-compare} Comparison between a prior from Eq.\
  \eqref{eq:uniform-source-frame} using \texttt{Planck15} parameters (see text)
  and the rational function approximation in Eq.\ \eqref{eq:rf-dL-prior}.  The
  priors are identical to the eye.  The range of luminosity distance corresponds
  to $0 \leq z < 4$ for the chosen cosmological parameters.}
%
\end{figure}

\acknowledgments I thank Maya Fishbach for reviewing the code associated with
this technical note.  This document is LIGO Document
\href{https://dcc.ligo.org/T1900192/public}{T1900192}.

\software{IPython \citep{2007CSE.....9c..21P}, NumPy
\citep{OliphantNumPy,2011CSE....13b..22V}, SciPy \citep{SciPy}, Matplotlib
\citep{2007CSE.....9...90H}, Seaborn, Astropy \citep{astropy:2013,astropy:2018}}

\bibliography{approx-prior}

\end{document}
