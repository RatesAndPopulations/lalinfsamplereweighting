# Re-weighting of LALInference samples into the comoving frame.
# Copyright (C) 2019 Will M. Farr <will.farr@ligo.org>, Daniel Wysocki
# <daniel.wysocki@ligo.org>, Richard O'Shaughnessy <richard.oshaughnessy@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

r"""This module contains functions to re-weight samples from LALInference, which
uses a prior that is flat in source-frame masses and proportional to
:math:`d_L^2` into the comoving frame.  The reweighting also includes a
:math:`1+z` factor to account for the  redshifting of time (i.e. the rate)
between the source and detector frame.  TL;DR: if you produce a rate density

.. math::

    \frac{\mathrm{d} N}{\mathrm{d} m_1^{\mathrm{source}} \mathrm{d} m_2^{\mathrm{source}} \mathrm{d} V_C \mathrm{d} t^{\mathrm{source}}}

then you can apply these weights to LALInference samples and obtain samples
drawn from the LI likelihood function in your (source-frame) parameter space.

The necessary re-weighting factor is

.. math::

    w \equiv \frac{1}{p_{LI}\left( m_1^\mathrm{det}, m_2^\mathrm{det}, d_L \right) } \left| \frac{\partial \left(m_1^{\mathrm{source}}, m_2^{\mathrm{source}}, V_C, t^{\mathrm{source}} \right)}{\partial \left( m_1^{\mathrm{det}}, m_2^{\mathrm{det}}, d_L, t^{\mathrm{det}} \right)} \right|

where :math:`p_{LI}` is the LALInference prior density, and :math:`V_C` is the
comoving volume.  This factor is required so that subsequent analyses can impose
their priors in the comoving frame (i.e. priors in :math:`m_1^\mathrm{source}`,
:math:`m_2^\mathrm{source}`, :math:`V_C`, and :math:`t^\mathrm{source}`), and
they will automatically be converted to the appropriate density for the LI
sampling in the :math:`m_1^\mathrm{det}`, :math:`m_2^\mathrm{det}`, :math:`d_L`
space.

The Jacobian is triangular, so can be implemented as a product of four factors

.. math::

    \frac{\partial m_1^{\mathrm{source}}}{\partial m_1^{\mathrm{det}}} \frac{\partial m_2^{\mathrm{source}}}{\partial m_2^{\mathrm{det}}} \frac{\partial V_C}{\partial d_L} \frac{\partial t^{\mathrm{source}}}{\partial t^\mathrm{det}}

The first two factors are just inverse :math:`1+z`, the third factor can be
derived from

.. math::

    \frac{\partial V_C}{\partial d_L} = \frac{\partial V_C}{\partial z} \left[ \frac{\partial d_L}{\partial z} \right]^{-1}

where

.. math::

    \frac{\partial d_L}{\partial z} = d_C + \left( 1+z\right)\frac{d_H}{E(z)}

in a flat universe (:math:`d_C` is the comoving distance, :math:`d_H` is the
Hubble distance, and :math:`E(z)` is the standard integrand; see `Hogg (1999)
<https://ui.adsabs.harvard.edu/#abs/1999astro.ph..5116H/abstract>`_ for more
information), and the fourth factor is another inverse :math:`1+z`.

"""

import approxprior
from numpy import expm1, log1p, linspace, log, pi

from astropy.cosmology import Planck15
import astropy.cosmology as cosmo
import astropy.units as u
from scipy.interpolate import interp1d
import warnings

def log_lirewt_factor(li_dls, cosmology=Planck15, distance_unit=u.Mpc, dl2prior=True):
    """Returns the log of an un-normalzed re-weighting factor for LALInference
    samples to the comoving frame.

    :param li_dls: Luminosity distance samples from lalinference.

    :param cosmology: The cosmology to use to perform the re-weighting. (Default is `Planck15`.)

    :param distance_unit: The distance unit associated to li_dls (default `Mpc`.)

    :param dl2prior: If ``True`` (the default), then assume the LALInference
      samples use a :math:`d_L^2` prior.  If ``False``, assume that the prior
      follows the approximate uniform-in-source-frame merger rate prior from
      ``approxprior``.

    :return: An array of the same shape as li_dls giving the log of the
      re-weighting factor (:math:`w` above) to make the samples uniform in the
      comoving frame.

    """
    if not cosmology is Planck15:
        warnings.warn("if you use re-weighting with a custom cosmology, it *must* be spatially flat (sum(Omega) == 1)")

    nz = 1000
    zmax = 10
    zs_interp = expm1(linspace(log(1), log(1+zmax), nz))
    dl_interp = cosmology.luminosity_distance(zs_interp).to(distance_unit).value

    li_zs = interp1d(dl_interp, zs_interp)(li_dls)

    li_dVdz = 4.0*pi*cosmology.differential_comoving_volume(li_zs).to(distance_unit**3/u.sr).value
    li_dc = cosmology.comoving_distance(li_zs).to(distance_unit).value

    li_dzddl = 1.0/(li_dc + (1+li_zs)*cosmology.hubble_distance.to(distance_unit).value/cosmology.efunc(li_zs))

    if dl2prior:
        log_li_prior = 2.0*log(li_dls)
    else:
        log_li_prior = log(approxprior.approx_dl_prior(li_dls, distance_unit=distance_unit))

    # weight is 1.0/(1+z)^2 dV/d(dL) / dl^2 * 1/(1+z)
    log_rewt = -3.0*log1p(li_zs) + log(li_dVdz) + log(li_dzddl) - log_li_prior

    return log_rewt
