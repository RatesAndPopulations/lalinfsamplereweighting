#!/bin/bash

# Empty file that will store checksums
> checksums.log

# Approximants used
approxes="IMRPhenomPv2 SEOBNRv3"

# Ensure directories exist for all of the posteriors to be fetched.
mkdir -p $approxes

# Fetch the posterior samples for all approximants.
./sample_fetcher.sh IMRPhenomPv2/ < sample_list_IMRP.txt
./sample_fetcher.sh SEOBNRv3/ < sample_list_SEOB.txt

# Ensure output directories exist for all weights.
mkdir -p IMRPhenomPv2_weights_only SEOBNRv3_weights_only

# Compute the weighting factors for each set of posterior samples.
./get_sample_weights.py

# Ensure output directories exist for reweighted posteriors.
mkdir -p IMRPhenomPv2_reweighted_fix SEOBNRv3_reweighted_fix

# Combine the original posterior samples with the new "weight" field for every
# set of posterior samples.
for approx in $approxes
do

    for event_file in $approx/*
    do
    # Get just the part of the filename pertinant to the event.
	event_basename=$(basename $event_file)

    # Concatenates the original posterior samples file with the new file that
    # only contains weights.  Concatenation is done "horizontally", so the new
    # file is appended as an extra column.
	paste \
	    "${approx}/${event_basename}" \
	    "${approx}_weights_only/${event_basename}" \
	    > "${approx}_reweighted_fix/${event_basename}"

    # Compute checksums for original file, new weights file, and new combined file.
    for suffix in "" "_weights_only" "_reweighted_fix"
    do
        md5sum "${approx}${suffix}/${event_basename}" >> checksums.log
    done

    done
done
