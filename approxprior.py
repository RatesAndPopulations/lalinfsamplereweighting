# Prior in dL approximating uniform-in-source-frame merger rate.
# Copyright (C) 2019 Will M. Farr <will.farr@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from numpy import pi
import astropy.units as u

def approx_dl_prior(dl, distance_unit = u.Mpc):
    r""" Approximate prior in :math:`d_L` for a uniform-in-source-frame merger rate.

    The approximation is given by

      .. math::

        \frac{\mathrm{d} N}{\mathrm{d} d_L} \sim 4 \pi \frac{d_L^2}{c_0 + c_1 d_{L,9} + c_2 d_{L,9}^2 + c_3 d_{L,9}^3 + c_4 d_{L,9}^4}

    where :math:`d_{L,9}` is the luminosity distance in Gpc, and the fit
    approximates the cosomolgy in `Planck15` with

      .. math::

        c = \left(1.012306, 1.136740, 0.262462, 0.016732, 0.000387\right)

    :return: The prior weight which is a good approximation a prior that
      corresponds to a uniform merger rate in the source frame for :math:`0 \leq z
      < 4` (i.e. for aLIGO ranges).

"""

    c = [ 1.012306, 1.136740, 0.262462, 0.016732, 0.000387 ]

    dl9 = (dl*distance_unit).to(u.Gpc).value

    return 4.0*pi*dl*dl/(c[0] + dl9*(c[1] + dl9*(c[2] + dl9*(c[3] + dl9*c[4]))))
