#!/bin/bash
#
# Usage: ./sample_fetcher.sh $destination < $sample_list
#
# Downloads a set of posterior sample files, storing them at $destination.
# Takes the files specified through stdin, which should be formatted as a line
# for each file, which should contain rows: repo name, file name, commit.  The
# entries must be delimited by a single space.  The repo is assumed to be a
# sub-repository of the group `pe_event_samples` on git.ligo.org.  So for
# instance, to download the IMRPhenomPv2 posteriors for GW150914 and GW151226,
# one could run the command with the following input:
#
# GW150914 rerun_O2_catalog/allIsp_post.dat bcb854bfb9610017d65e2c4b746ac141ece1a87b
# GW151226 rerun_O2_catalog/allIMRPPsp_O2cat_post.dat 16756cdc6c777c6c04d3c520ed40745a81e107b9
#
# This will download the file rerun_O2_catalog/allIsp_post.dat, from the given
# commit, from the repository `git@git.ligo.org:pe_event_samples/GW150914.git`,
# and similarly for GW151226.  The files will be downloaded via git lfs, without
# fetching any other large files unnecessarily.  Since the filenames themselves
# likely clash, as they do in this example, they will have the repository name
# prepended to them, with a dot separating them.  Also, any sub-directories
# within the repository will be flattened.  So the files in this case would be
# stored as `$destination/GW150914.allIsp_post.dat` and
# `$destination/allIMRPPsp_O2cat_post.dat`.


destination=$(readlink -f "${1}")

# Move into a temporary directory where we'll clone the repos
tmp_dir=$(mktemp -d)
cd "${tmp_dir}"

process_entry(){
    # Read file columns into variables, and use them
    echo "$1" | while IFS=' ' read repo_name file_name commit
    do
        # Document where we are in the input file.
        >&2 echo \
            "Repository: ${repo_name}; File: ${file_name}; Commit: ${commit}"

        # Skip files that exist already.
        if [ -f "${destination}/${repo_name}.${file_name}" ]
        then
            >&2 echo "File exists"
            continue
        fi

        # Clone the repo, keeping LFS files as just pointers.
        GIT_LFS_SKIP_SMUDGE=1 \
            git clone "git@git.ligo.org:pe_event_samples/${repo_name}.git"

        # Move into the repository
        cd "${repo_name}"

        # Checkout the desired commit.
        git checkout "${commit}"

        # Resolve the LFS pointer for the desired file.
        git lfs pull --include "${file_name}"

        # Move the desired file to the destination folder, prepending the repo
        # name to the filename to remove ambiguity.
        mv "${file_name}" "${destination}/${repo_name}.dat"

        # Move back into the temporary directory
        cd ..

        # Delete the repository
        rm -rf "${repo_name}"
    done
}

while IFS= read -r line;
do
    process_entry "${line}"
done

# Delete the temporary directory
rm -rf "${tmp_dir}"
