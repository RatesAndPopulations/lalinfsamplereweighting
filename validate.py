from pylab import *

import numpy
import numpy as np

import astropy.cosmology as cosmo
from astropy.cosmology import Planck15
import astropy.units as u

import emcee

from scipy.interpolate import interp1d

import seaborn as sns

import os

import rewtlisamples
import rewtlisamples_old

reweight_modules = {
    "fixed" : rewtlisamples,
    "old" : rewtlisamples_old,
}

sns.set_context('notebook')
sns.set_style('ticks')
sns.set_palette('colorblind')

seed = 128
random = numpy.random.RandomState(seed)


try:
    os.makedirs("review_img")
except OSError:
    pass


class UniformPosterior(object):
    def __init__(self, mmin, mmax, dlmax):
        self.mmin = mmin
        self.mmax = mmax
        self.dlmax = dlmax

        self.zinterp = expm1(linspace(log(1), log(10+1), 1000))
        self.dlinterp = Planck15.luminosity_distance(self.zinterp).to(u.Mpc).value

    def __call__(self, m1m2dl):
        m1d, m2d, dl = m1m2dl

        if m2d > m1d:
            return np.NINF
        if m2d < self.mmin:
            return np.NINF
        if m1d > self.mmax:
            return np.NINF
        if dl > self.dlmax:
            return np.NINF
        if dl < 0:
            return np.NINF

        logprior = 2.0*log(dl)

        return logprior

def check_mass_cdf(filename, m1, m2, mmin, mmax, cdf_label="expected"):
    m1m2 = numpy.concatenate((m1, m2))

    fig, (ax_m1, ax_m2, ax_both) = plt.subplots(
        nrows=3,
        sharex=True, figsize=[8,15],
    )

    n_samples = m1.size
    m_grid = numpy.linspace(mmin, mmax, 500)

    cdf_m1 = 2.0 / numpy.square(mmax - mmin) * (
        0.5 * (numpy.square(m_grid) - numpy.square(mmin)) -
        mmin * (m_grid - mmin)
    )
    ecdf_m1 = numpy.asarray([
        numpy.count_nonzero(m1 <= m) / n_samples
        for m in m_grid
    ])
    KS_m1 = numpy.max(numpy.abs(ecdf_m1 - cdf_m1))

    ax_m1.plot(m_grid, cdf_m1, color="black", label=cdf_label)
    ax_m1.plot(m_grid, ecdf_m1, color="C3", linestyle="--", label="actual")

    cdf_m2 = 2.0 / numpy.square(mmax - mmin) * (
        mmax * (m_grid - mmin) -
        0.5 * (numpy.square(m_grid) - numpy.square(mmin))
    )
    ecdf_m2 = numpy.asarray([
        numpy.count_nonzero(m2 <= m) / n_samples
        for m in m_grid
    ])
    KS_m2 = numpy.max(numpy.abs(ecdf_m2 - cdf_m2))

    ax_m2.plot(m_grid, cdf_m2, color="black", label=cdf_label)
    ax_m2.plot(m_grid, ecdf_m2, color="C3", linestyle="--", label="actual")

    cdf_both = (m_grid - mmin) / (mmax - mmin)
    ecdf_both = numpy.asarray([
        numpy.count_nonzero(m1m2 <= m) / (2*n_samples)
        for m in m_grid
    ])
    KS_both = numpy.max(numpy.abs(ecdf_both - cdf_both))

    ax_both.plot(m_grid, cdf_both, color="black", label=cdf_label)
    ax_both.plot(m_grid, ecdf_both, color="C3", linestyle="--", label="actual")

    ax_m1.set_ylabel("CDF ($m_1$)")
    ax_m2.set_ylabel("CDF ($m_2$)")
    ax_both.set_ylabel("CDF ($m_1$ and $m_2$)")

    ax_both.set_xlabel("mass [Msun]")

    fig.tight_layout()

    fig.savefig(filename)
    plt.close(fig)

    print(
        "{filename} KS tests: m1 = {m1}, m2 = {m2}, both = {both}"
        .format(filename=filename, m1=KS_m1, m2=KS_m2, both=KS_both)
    )


def check_vt_dist(filename_fmt_str, V, cdf_label="expected"):
    # Demonstrate that unweighted comoving volume samples are non-uniform.
    n_samples = V.size
    V_max = V.max()
    V_smooth = linspace(0.0, V_max, 1000)

    cdf_expected = V_smooth / V_max

    ecdf = asarray([
        count_nonzero(V <= Vs) / n_samples
        for Vs in V_smooth
    ])
    KS_V_c = np.max(np.abs(cdf_expected - ecdf))

    fig, ax = plt.subplots()
    sns.distplot(V, ax=ax)
    ax.set_xlabel("V_c")
    ax.set_ylabel("p(V_c)")
    fig.savefig(filename_fmt_str.format("pdf"))
    plt.close(fig)

    fig, ax = plt.subplots()
    ax.plot(V_smooth, cdf_expected, color="black", label="desired")
    ax.plot(V_smooth, ecdf, color="C3", linestyle="--", label="actual")
    ax.set_xlabel("V_c")
    ax.set_ylabel("cumulative prob")
    ax.legend(loc="upper left")
    fig.savefig(filename_fmt_str.format("cdf"))
    plt.close(fig)

    print(
        "{filename} KS test: V_c = {V_c}"
        .format(filename=filename_fmt_str, V_c=KS_V_c)
    )




# We instantiate a posterior object, draw some initial conditions, and construct
# a sampler:
logpost = UniformPosterior(5, 100, 5000)

m1 = random.uniform(low=5, high=100, size=128)
m2 = random.uniform(low=5, high=100, size=128)

mh = np.where(m1 > m2, m1, m2)
ml = np.where(m1 > m2, m2, m1)

dl = random.uniform(low=0, high=5000, size=128)

pts = column_stack((mh, ml, dl))


sampler = emcee.EnsembleSampler(128, 3, logpost)
# Now we run the sampler for 16384 steps (burnin), and then reset it.
samples, _, _ = sampler.run_mcmc(pts, 16384)
sampler.reset()

# Now we run for 16384 samples (proper sampling).
sampler.run_mcmc(samples, 16384);

# The samples look like they correspond to the prior
# (flat in detector frame, dL^2)
fig, ax = subplots()
img = ax.hexbin(
    sampler.chain[:,:,0].flatten(),
    sampler.chain[:,:,1].flatten(),
    mincnt=1,
)
cbar = colorbar(img)
ax.set_xlabel("m1")
ax.set_ylabel("m2")
cbar.set_label("prob density")

fig.savefig("review_img/m1_pdf_unweighted.png")
plt.close(fig)


# Confirm this with a KS test, and plot mass CDF's
check_mass_cdf(
    "review_img/mass_cdf_unweighted.png",
    sampler.chain[...,0].flatten(), sampler.chain[...,1].flatten(),
    5.0, 100.0,
)


# Show that p(d_L) is proportional to d_L^2
fig, ax = plt.subplots()
sns.distplot(sampler.chain[:,:,2].flatten(), ax=ax)
dLs = linspace(0, 5000, 1000)

ax.plot(dLs, 3.0*dLs*dLs/(5000*5000*5000))
ax.set_xlabel("dL")
ax.set_ylabel("p(dL)")

fig.savefig("review_img/dL_pdf_unweighted.png")
plt.close(fig)


# Obtain unweighted distance, redshift, and comoving volume posteriors.
d = sampler.chain[:,:,2].flatten()

zi = linspace(0, 3, 1000)
di = Planck15.luminosity_distance(zi).to(u.Mpc).value
z = interp1d(di, zi)(d)

V = Planck15.comoving_volume(z).to(u.Mpc**3).value


check_vt_dist("review_img/Vc_{}_unweighted.png", V)

d_unweight = sampler.chain[:,:,2]

for name, reweight_module in reweight_modules.items():
    logwts = reweight_module.log_lirewt_factor(d_unweight)
    logwts -= np.max(logwts) # Normalize to maxwt = 1
    logrs = log(random.uniform(low=0, high=1, size=logwts.shape))
    sel = logrs < logwts
    rewt_samples = sampler.chain[sel]

    d = rewt_samples[:,2]
    zi = linspace(0, 3, 1000)
    di = Planck15.luminosity_distance(zi).to(u.Mpc).value
    z = interp1d(di, zi)(d)

    V = Planck15.comoving_volume(z).to(u.Mpc**3).value

    m1 = rewt_samples[:,0]/(1+z)
    m2 = rewt_samples[:,1]/(1+z)

    cut = (m2 > 10) & (m1 < 50)


    # Plot the mass samples.
    fig, ax = plt.subplots()
    ax.hexbin(m1[cut],m2[cut],mincnt=1)
    ax.set_xlabel("m1_source")
    ax.set_ylabel("m2_source")
    fig.savefig("review_img/m1_pdf_weighted_{}.png".format(name))
    plt.close(fig)

    # Confirm this with a KS test, and plot mass CDF's
    check_mass_cdf(
        "review_img/mass_cdf_weighted_{}.png".format(name),
        m1[cut], m2[cut], 10.0, 50.0,
    )

    # Check whether the the comoving volume distribution is uniform
    check_vt_dist(
        "review_img/Vc_{{}}_weighted_{}.png".format(name),
        V[cut],
        cdf_label="desired",
    )

    # Show that p(d_L) is no longer proportional to d_L^2
    fig, ax = plt.subplots()
    sns.distplot(d[cut], ax=ax)
    dLs = linspace(0, 5000, 1000)

    ax.plot(dLs, 3.0*dLs*dLs/(5000*5000*5000))
    ax.set_xlabel("dL")
    ax.set_ylabel("p(dL)")

    fig.savefig("review_img/dL_pdf_weighted_{}.png".format(name))
    plt.close(fig)
